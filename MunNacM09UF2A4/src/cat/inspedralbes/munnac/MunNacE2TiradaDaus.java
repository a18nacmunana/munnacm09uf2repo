package cat.inspedralbes.munnac;

import java.util.ArrayList;

public class MunNacE2TiradaDaus {
	public static void main(String[] args) {
		ArrayList <Thread> tirades = new ArrayList<Thread>();
		
		for(int i=0;i<Integer.parseInt(args[0]);i++){
			tirades.add(new Thread (new MunNacE2RunnableTirador()));
		}
		
		for(int i=0 ; i<tirades.size();i++){
			Thread tirada=tirades.get(i);
			tirada.setName("Num "+i+": ");
			if(i==3)
				tirada.setPriority(10);
			else
				tirada.setPriority(1);
			tirada.start();
		}
		
		for(int i=0 ; i<tirades.size();i++){
			Thread tirada=tirades.get(i);
			try {
				tirada.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Tirada acabada");
	}
}
