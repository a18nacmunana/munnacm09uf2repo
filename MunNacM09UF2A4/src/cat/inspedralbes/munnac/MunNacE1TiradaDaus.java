package cat.inspedralbes.munnac;

import java.util.ArrayList;
import java.util.Random;

public class MunNacE1TiradaDaus{
	/*
	 *	a) Si s'invoca el mètode run en comptes del start, s'executaria 
	 *	de manera seqüencial perque va directament al métode
	 *
	 *	b) S'executa tot el main sense esperar els fils.
	 *
	 *	c) Que es transforma en seqüencial perque no s'executa un fins que acaba l'anterior
	 */
	public static void main(String[] args) {
		ArrayList <Thread> tirades = new ArrayList<Thread>();
		
		for(int i=0;i<Integer.parseInt(args[0]);i++){
			tirades.add(new MunNacE1TiradorSenzill());
		}
		
		for(int i=0 ; i<tirades.size();i++){
			Thread tirada=tirades.get(i);
			tirada.setName("Num "+i+": ");
			if(i==3)
				tirada.setPriority(10);
			else
				tirada.setPriority(1);
			tirada.start();
		}
		
		for(int i=0 ; i<tirades.size();i++){
			Thread tirada=tirades.get(i);
			try {
				tirada.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Tirada acabada");
	}
}
