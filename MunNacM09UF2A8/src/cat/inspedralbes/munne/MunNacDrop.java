package cat.inspedralbes.munne;

import java.util.ArrayList;

public class MunNacDrop {
    // Message sent from producer
    // to consumer.
    ArrayList<String> message;
    int qMessages;
    // True if consumer should wait
    // for producer to send message,
    // false if producer should wait for
    // consumer to retrieve message.
    
    MunNacDrop(int q){
    	this.qMessages=q;
    	message = new ArrayList();
    }
    
    public synchronized String take() {
        // Wait until message is
        // available.
        while (message.size()<1) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        // Toggle status.
        
        // Notify producer that
        // status has changed.
        String msg = this.message.remove(0);
        
        System.out.println(message.toString());
        notifyAll();
        return msg;
    }

    public synchronized void put(String message) {
        // Wait until message has
        // been retrieved.
        while (this.message.size()>=qMessages) {
            try { 
                wait();
            } catch (InterruptedException e) {}
        }
        this.message.add(message);
        // Toggle status.
    
        // Notify consumer that status
        // has changed.
        System.out.println(this.message.toString());
        notifyAll();
    }
}
