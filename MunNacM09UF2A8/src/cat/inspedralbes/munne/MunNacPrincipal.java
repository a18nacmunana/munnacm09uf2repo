package cat.inspedralbes.munne;

public class MunNacPrincipal {

	public static void main(String[] args) {
		MunNacDrop drop = new MunNacDrop(3);
        Thread producer = new Thread(new MunNacProducer(drop));
        Thread consumer = new Thread(new MunNacConsumer(drop));
        producer.start();
        consumer.start();
	}
}