package cat.inspedralbes.munne;

import java.util.Random;

public class MunNacProducer implements Runnable {
	
    private MunNacDrop drop;

    public MunNacProducer(MunNacDrop drop) {
        this.drop = drop;
    }

    public void run() {
        String importantInfo[] = {
            "1",
            "2",
            "3",
            "4",
            "5"
        };
        Random random = new Random();
        for (int i = 0;i < importantInfo.length;i++) {
            drop.put(importantInfo[i]);
            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {}
        }
        drop.put("DONE");
    }
}
