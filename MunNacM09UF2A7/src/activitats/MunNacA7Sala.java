package activitats;

public class MunNacA7Sala {
	int capacity;
	int freeSpace;
	
	MunNacA7Sala(int c){
		this.capacity=c;
		this.freeSpace=c;
	}
	
	public synchronized void getIn() {
		while(freeSpace<1) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		freeSpace--;
		System.out.println(Thread.currentThread().getName() + " ha ENTRAT a la sala");
		System.out.println("Espai lliure: "+ freeSpace);
	}
	
	public synchronized void getOut() {
		System.out.println(Thread.currentThread().getName() + " ha SORTIT de la sala");
		freeSpace++;
		notifyAll();
		System.out.println("Espai lliure: "+ freeSpace);

	}
	
}
