package activitats;

import java.util.Random;

public class MunNacA7Persona extends Thread{
	private MunNacA7Sala sala;
	MunNacA7Persona(MunNacA7Sala sala){
		this.sala=sala;
	}
	
	public void run(){
		Random rand = new Random();
		try {
			Thread.sleep((rand.nextInt(5)+1)*1000);			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sala.getIn();
		try {
			Thread.sleep((rand.nextInt(5)+1)*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sala.getOut();
	}
}
