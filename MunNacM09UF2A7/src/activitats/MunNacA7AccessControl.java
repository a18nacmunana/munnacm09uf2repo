package activitats;

import java.util.ArrayList;

public class MunNacA7AccessControl {

	public static void main(String[] args) {
		MunNacA7Sala sala= new MunNacA7Sala(10);
		ArrayList <Thread> persones = new ArrayList<Thread>();

		for(int i=0; i<30;i++) {
			Thread persona = new MunNacA7Persona(sala);
			persona.setName("Persona "+(i+1));
			persones.add(persona);
		}
		
		for(int i=0; i<persones.size();i++) {
			Thread persona = persones.get(i);
			persona.start();
		}
		for(int i=0; i<persones.size();i++) {
			Thread persona = persones.get(i);
			try {
				persona.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
