package cat.inspedralbes.munne;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class MunNacE1Deb {

	public static void main(String[] args) {
	   try {
		   	Scanner scan = new Scanner(System.in);
		   	System.out.print("Introdueix la ruta del deb: ");
		   	String ruta = scan.nextLine();
			ProcessBuilder pb = new ProcessBuilder("dpkg", "-b", ruta, ".");
			Process p = pb.start();
			p.waitFor();
			String nom="";
			String version="";
			String arch="";
			String l;
			BufferedReader r = new BufferedReader(new FileReader(ruta+"/DEBIAN/control"));
			while ((l = r.readLine()) != null) {
				if(l.startsWith("Package: "))
					nom=l.split(" ")[1];
				else if(l.startsWith("Version: "))
					version=l.split(" ")[1];
				else if(l.startsWith("Architecture: "))
					arch=l.split(" ")[1];
			}
			r.close();
			pb = new ProcessBuilder("lintian", "-c", "/dades/dades/Nacho/M09/munnacm09uf2repo/MunNacM09UF2A3/"+nom+"_"+version+"_"+arch+".deb");
			p=pb.start();
			
			String result="";
			r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((l = r.readLine()) != null) 
				result+=l+"\n";
			r.close();
			p.waitFor();
			int eValue=p.exitValue();
			if(eValue==0) 
				System.out.println("El deb és correcte");
			
			else {
				System.out.print(result);
				System.out.println("---------------------\nEl deb és incorrecte\n---------------------");
				System.out.println("Exit value: "+ eValue);
				pb= new ProcessBuilder("rm", "/dades/dades/Nacho/M09/munnacm09uf2repo/MunNacM09UF2A3/"+nom+"_"+version+"_"+arch+".deb");
				pb.start();
			}
			
		}catch (IOException | InterruptedException e) {
				e.printStackTrace();
		}
	}
}

