package activitats;

import java.util.logging.Level;
import java.util.logging.Logger;

class CogNomA5Hotel {	
	
	private final static Logger LOGGER = Logger.getLogger(CogNomA5Hotel.class.getName());
	
	private final String name;															
	private final CogNomA5Address address;												
	private final int rooms;															
	private int freeRooms;		
	public CogNomA5Hotel(String n, CogNomA5Address a,int r){
		name=n;
		address=a;
		rooms=r;
	}	
	public String getName() {
		return name;
	}
	public CogNomA5Address getAddress() {
		return address;
	}
	public int getRooms() {
		return rooms;
	}	
	public synchronized int getFreeRooms(){
		return freeRooms;
	}
	public synchronized void setFreeRooms(int fr){
		freeRooms=fr;
		LOGGER.log(Level.INFO,"Free rooms: "+freeRooms);
	}
	public synchronized boolean getAvailability(int numRooms){
		boolean value=false;
		if (numRooms<=freeRooms)
			value=true;
		LOGGER.log(Level.INFO, Thread.currentThread().getName() + " wants "+numRooms+" rooms: AVAILABILITY "+value);		
		return value;		
	}
	public synchronized double getOccupancyRate(){
		return (double)(rooms-getFreeRooms())/rooms;
	}	
	public synchronized void processPetition(int numRooms){ // book: numRooms>0, cancellation: numRooms<0
		freeRooms = freeRooms - numRooms;
		LOGGER.log(Level.INFO, Thread.currentThread().getName() + ": BOOKING "+numRooms+" ROOMS"); 
		LOGGER.log(Level.INFO, Thread.currentThread().getName() + ": call ends: "+freeRooms+ " AVAILABLE");		
	}		
	public synchronized void bookingPetition(int numRoomsToBook){
		if(getAvailability(numRoomsToBook))					
			processPetition(numRoomsToBook);
	}
}

class CogNomA5Address {
	private String street; 
	private String city;
	private String province;
	private String country;
	private String zip;	
	
	public CogNomA5Address(String s,String c,String z)
	{
		street=s;
		city=c;
		province=c;
		zip=z;
		country="Spain";
	}
	
	public CogNomA5Address(String s,String c,String p,String z)
	{
		street=s;
		city=c;
		province=p;
		zip=z;
		country="Spain";
	}	
	public CogNomA5Address(String s,String c,String p,String z,String co)
	{
		street=s;
		city=c;
		province=p;
		zip=z;
		country=co;
	}	
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}	
}


