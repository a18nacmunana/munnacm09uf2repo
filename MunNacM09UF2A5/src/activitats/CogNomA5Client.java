package activitats;

class CogNomA5Client extends Thread{	
	private CogNomA5Hotel hr;
	private int rooms;	
	private int opcio;
	public CogNomA5Client(CogNomA5Hotel hotelRooms,int numRoomsToBook, int opcio)
	{
		hr=hotelRooms;
		rooms=numRoomsToBook;
		this.opcio=opcio;
	}	
	public void run(){		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(opcio==1)
			hr.bookingPetition(rooms);
		else if (opcio==2)
			hr.getAvailability(rooms);
		else if (opcio==3)
			System.out.println(hr.getFreeRooms());
		else if (opcio==4)
			System.out.println(hr.getOccupancyRate());
	}
}
