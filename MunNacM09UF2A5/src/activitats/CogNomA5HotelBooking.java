package activitats;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CogNomA5HotelBooking {
	
	private final static Logger LOGGER = Logger.getLogger(CogNomA5HotelBooking.class.getName());
	
	public static void main(String[] args) throws InterruptedException {		
		
		LOGGER.setLevel(Level.INFO);
				
		CogNomA5Hotel hr= new CogNomA5Hotel("IAM",new CogNomA5Address("Avinguda d'Esplugues, 38","Barcelona","08034"),40);		
		hr.setFreeRooms(5);			
		
		CogNomA5Client client1 = new CogNomA5Client(hr,4,1);		
		CogNomA5Client client2 = new CogNomA5Client(hr,2,1);	
		CogNomA5Client client3 = new CogNomA5Client(hr,1,3);
		CogNomA5Client client4 = new CogNomA5Client(hr,1,4);
		CogNomA5Client client5 = new CogNomA5Client(hr,1,2);
		
		client1.setName("Client A");				
		client2.setName("Client B");			
		client3.setName("Client C");			
		client4.setName("Client D");
		client5.setName("Client E");
		
		client1.start();		
		client2.start();
		client3.start();
		client4.start();
		client5.start();
	}
}
