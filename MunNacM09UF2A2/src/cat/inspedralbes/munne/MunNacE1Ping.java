package cat.inspedralbes.munne;

import java.io.IOException;

public class MunNacE1Ping {
	
	//Què passa si oblidem esperar que s’acabi el procés?
	
	//RESPOSTA: Només fa print el primer ping perque el main acaba abans que el procés del ping.
	
	public static void main(String[] args) {
		ProcessBuilder pb = new ProcessBuilder("ping","-c 4","google.com");
		pb.inheritIO();
		try {
			Process p = pb.start();
			int returnCode=p.waitFor(); 
			int exitValue=p.exitValue();
			System.out.println(exitValue);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
