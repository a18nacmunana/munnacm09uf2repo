package cat.inspedralbes.munne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MunNacE2Ls {
	public static void main(String[] args) {
		ProcessBuilder pb = new ProcessBuilder("ls","-la");
		try {
			Process p = pb.start();
			String l;
			int i=0;
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((l = bri.readLine()) != null) {
		        System.out.println(i +". "+ l);
		        i++;
		      }
			bri.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
