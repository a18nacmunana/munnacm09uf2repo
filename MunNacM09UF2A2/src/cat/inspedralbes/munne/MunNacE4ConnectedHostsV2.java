package cat.inspedralbes.munne;

import java.io.IOException;
import java.util.ArrayList;

public class MunNacE4ConnectedHostsV2 {
	static String IP= "192.168.205.1";
	public static void main(String[] args) {
		String ip;
		int cont=0;
		int exitValue;
		ArrayList <Process> processos = new ArrayList <Process>();
		ArrayList <String> ips = new ArrayList <String>();
		for(int i=0; i<35;i++) {
			if(i<10)
				ip=IP+0+i;
			else
				ip = IP+i;
			ProcessBuilder pb = new ProcessBuilder("ping", "-c 1",ip);
			ips.add(ip);
			try {
				processos.add(pb.start());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		while(processos.size()!=0) {
			for(int i=0; i<processos.size();i++) {
				Process p = processos.get(i);
				if(!p.isAlive()) {
					processos.remove(i);
					if(p.exitValue()==0) {
						cont++;
						System.out.println(ips.get(i) + " està connectat");
						ips.remove(i);
					}
				}
			}
		}
		System.out.println("--------------\n" + cont + " equips connectats");
	}

}
