package cat.inspedralbes.munne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MunNacE4ConnectedHosts {
	static String IP= "192.168.205.1";
	public static void main(String[] args) {
		int cont=0;
		String ip;
		
		for(int i=0; i<35;i++) {
			if(i<10)
				ip=IP+0+i;
			else
				ip = IP+i;
			ProcessBuilder pb = new ProcessBuilder("ping", "-c 1",ip);
			try {
				Process p = pb.start();		
				String l;
				BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((l = bri.readLine()) != null)
					if(l.startsWith("64")) {
						cont++;
						System.out.println(ip + " està conectat");
					}
				bri.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Hi ha "+cont+" ordinadors connectats");
	}
}
