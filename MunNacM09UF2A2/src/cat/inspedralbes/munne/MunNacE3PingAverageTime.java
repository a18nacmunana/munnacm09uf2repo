package cat.inspedralbes.munne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class MunNacE3PingAverageTime {
	/*Si es tanca el procés del java, també es tanca el procés ping.
	 * Si es tanca el procés ping, el procés java termina amb exit value 143
	 * 
	 */
	public static void main(String[] args) {
		int pings;
		if(args[1]==null) {
			System.out.println("Format erroni,  default=100 pings");
			pings=100;
		}
		else
			pings=Integer.parseInt(args[1]);
		System.out.println("Fent " +pings+" pings a " +args[3]);
		ProcessBuilder pb = new ProcessBuilder("ping","-c "+pings, args[3]);
		float temps=0;
		try {
			Process p = pb.start();
			String l;
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((l = bri.readLine()) != null) {
				if(l.startsWith("64"))
					temps+=Float.parseFloat(l.substring(l.length()-7,l.length()-3));
			}
			bri.close();
			System.out.println("La mitjana és: " + String.format("%.2f",temps/pings) +" ms");
			int exitValue=p.exitValue();
			System.out.println(exitValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
